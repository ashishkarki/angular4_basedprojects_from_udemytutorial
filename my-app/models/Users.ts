export interface IUser {
    name: string;
    age: number;
    city: string;
    isHuman: boolean;
    image?: string;
    isActive?: boolean;
    balance?: number;
    registered?: string;
    hide?: boolean; 
}