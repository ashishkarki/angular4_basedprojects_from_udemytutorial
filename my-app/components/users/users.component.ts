import { Component, OnInit } from '@angular/core';
import { IUser } from '../../models/Users';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: IUser[];
  currentClasses: {};
  currentStyles: {};

  constructor() { 
    this.users = new Array();
  }

  ngOnInit() {
    this.users = [
      {
        name: 'Jane Doe',
        age: 40,
        city: 'Andostris',
        isHuman: true,
        image: 'https://loremflickr.com/320/240',
        isActive: true,
        balance: 100,
        registered: '2018-01-24 08:30',
        hide:true
      },
      {
        name: 'Yaggy Yag',
        age: 100,
        city: 'Andostrimia',
        isHuman: false,
        image: 'https://loremflickr.com/320/240/dog',
        isActive: false,
        balance: 0,
        registered: '2017-11-32 13:30',
        hide: true
      }
    ];

    this.setCurrentClasses();
    this.setCurrentStyles();
  }

  setCurrentClasses() {
    this.currentClasses = {
      'btn-success': true,
      'big-text': true
    };
  }

  setCurrentStyles() {
    this.currentStyles = {
      'padding-top': false ? '0' : '30px',
      'background': false ? 'darkgray' : 'lightsalmon'
    };
  }

  getCurrentStyles() {
    this.currentStyles['font-family'] = 'cursive Sans-Serif';
    
    return this.currentStyles;
  }

  addUser(usrObj: IUser){
    console.log("button clicked with: ");
    this.users.push(usrObj);
  }
}
