import { Component } from '@angular/core';
import { log } from 'util';

@Component({
    selector: 'app-user'
    //, template: '<h2>Ashish Karki</h2>'
    , templateUrl: './user.component.html'
    , styles: [
        `
        h2 {
            font-style: italic;
        }
        `
    ]
    , styleUrls: ['./user.component.css']
})
export class UserComponent {
    firstName = 'Ashish';
    lastName = 'Doe';
    age = 30;
    address = {
        street: '100 main st',
        city: 'waltham',
        state: 'MA',
        country: 'usa'
    };

    constructor(){
        console.log('User Component is created........');
        console.log('Constructor is used to inject dependencies unlike onNgInit');
        this.sayHello();
    }

    sayHello(){
        console.log(`Hello ${this.firstName}`);
        let tups: myTuple[] = [];
        tups[0] = ['ashish', 32, true];
        console.log(tups[0]);
        
    }
}

export type myTuple = [string, number, boolean];